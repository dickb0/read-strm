extern crate image;

use std::fs::File;
use std::path::Path;

use byteorder::{BigEndian, ReadBytesExt};


fn main() {
    println!("Hello, world!");

    let mut data : Vec<STRMData> = Vec::new();

    let mut data_2d  = range_reader(52..59, 1..12);

    for lat in 52..56
    {
		for long in 1..7
        {
            let mut strm_data = STRMData::new(lat, long);

            strm_data.read_in();

            data.push(strm_data);

        }
    }
    
 
    // data.iter().for_each(|strm_data| strm_data.avgs_max());

    data.iter().for_each(|strm_data| strm_data.imageify());

}

fn range_reader(latitude : std::ops::Range<i16>, longitude: std::ops::Range<i16>) -> Vec<Vec<STRMData>>
{
    let mut _return : Vec<Vec<STRMData>> = Vec::new();

    for lat in latitude
    {
        let mut row : Vec<STRMData> = Vec::new();
        
        for long in longitude.start..longitude.end
        {
            let mut strm_data = STRMData::new(lat, long);
            strm_data.read_in();

            row.push(strm_data);

        }

        _return.push(row);
    }

    return _return;
}

enum Status { Good, Bad }

/// Store the heights for a 1 x 1 degree square
struct STRMData
{
    lat:i16, 
    long : i16,
    buffer: Vec<u16>,
    number_of_points: usize,

    status : Option<Status>
}

impl STRMData
{
    /// Create a new buffer for strm data
    pub fn new(lat:i16, long : i16) -> Self
    {
        let file_path = STRMData::file_path(lat, long);
        if let Ok(metadata) = file_path.metadata()
        {
            if let Ok(file_length) = usize::try_from(metadata.len())
            {
                let number_of_points = file_length / 2;
                let buffer  = vec![0; number_of_points];

                return STRMData{lat:lat, long:long, buffer:buffer, number_of_points:number_of_points, status: None}
            }
        }

        return STRMData{lat:lat, long:long, buffer:vec![0; 0], number_of_points:0, status: Some(Status::Bad)};
    }

    /// Read from file cache to memory
    pub fn read_in(& mut self) 
    {
        let file_path = STRMData::file_path(self.lat, self.long);

        match File::open(&file_path) 
        {
            Err(why) => eprintln!("couldn't open {}: {}", file_path.display(), why),
            Ok(mut file) => 
                    match file.read_u16_into::<BigEndian>(&mut self.buffer)
                        {
                            Err(what) => eprintln!("couldn't read {}: {}", file_path.display(), what),
                            Ok(_) => { self.status = Some(Status::Good);
                            eprintln!("read {} {} {}", self.lat, self.long, file_path.display())}
                        }

       };
    }

    /// Return the path to the cached file
    fn file_path(lat:i16, long : i16) -> std::path::PathBuf
    {
        const DATA_PATH: &'static str= "C:/Users/dickb/Documents/development/srtm-python/Data/N30/";
        let file_name = format!("N{}W{:03}.hgt", lat, long);

        return  Path::new(&DATA_PATH).join(file_name);
    }

    /// Print 
    pub fn _avgs_max(&self)
    {
        fn _folder(tup: (i32, i32), value:i32 ) -> (i32, i32) 
        {
            let _return = (tup.0 + value, std::cmp::max(tup.1, value));
            return _return;
        }
        let (sum, max) = self.buffer.iter().filter(|&&x | x < 8000u16 ).fold((0i32, 0i32), | acc, x | _folder(acc, *x as i32));

        let missing = self.buffer.iter().filter(|&&x | 8000u16 < x).count();

        if 0 < self.number_of_points
        {
            println!("lat {} long {} {} max {} avg {} missing {}", self.lat, self.long, self.buffer.len(), max, (sum / self.number_of_points as i32), missing);
        }
        else
        {
            println!("lat {} long {} {} max {} num points {}", self.lat, self.long, self.buffer.len(), max, self.number_of_points as i32)
        }

    }

    
    pub fn imageify(&self)
    {
        use image::{ ImageBuffer, RgbImage, Rgb};

        let bound = (self.number_of_points as f32).sqrt() as u32;

        let colour_map = make_colour_table();

use std::time::Instant;
let now = Instant::now();
       
        let mut img: RgbImage = ImageBuffer::new(bound, bound);

         for row in 0..1201 
        {
            for col in 0.. 1201
            {
    
                let height = self.buffer[row * 1201 + col];
                let colour = find_colour2(&colour_map, height);

                img.put_pixel(col as u32, row as u32, Rgb(*colour));
            }
        } 

        
let elapsed = now.elapsed();
println!("Make png elapsed: {:.2?}", elapsed);

        let file_name = format!("N{}W{:03}.png", self.lat, self.long);

        let path = Path::new("png\\").join(file_name);

        print!("path {}", path.display());

        match img.save(path)
        {
            Ok(_) => println!("Image saved ok "),
            Err(e) => println!("Image error  {}", e)
        }
    }
}





fn find_colour2(colour_map: &std::collections::HashMap<u16, [u8;3]>, height: u16) -> &[u8;3]
{
    let key = (height / 50) * 50;

     match colour_map.get(&key) 
     {
        Some(colour) => return colour,
        None =>  return &[255u8, 255u8, 255u8]
    };

}

fn make_colour_table() -> std::collections::HashMap<u16, [u8;3]>
{
    return std::collections::HashMap::from(
        [
            (0,   [127, 159, 101]),
            (50,  [160, 194, 124]),
            (100, [185, 214, 124]),
            (150, [207, 224, 156]),
            (200, [223, 233, 168]),
            (250, [241, 238, 166]),
            (300, [237, 223, 159]),
            (350, [240, 210, 141]),
            (400, [230, 188, 138]),
            (450, [216, 165, 133]),
            (500, [197, 149, 135]),
            (550, [217, 165, 156]),
            (600, [227, 183, 177]),
            (650, [223, 192, 191]),
            (700, [239, 205, 217]),
            (750, [244, 215, 225]),
            (800, [243, 223, 233]),
            (850, [248, 227, 232]),
            (900, [248, 233, 238]),
            (950, [246, 240, 245]),
        ]);
}